package ru.chelnokov.exams;

import java.util.ArrayList;
import java.util.Collections;


/**
 * Класс объекта Студент, его полей и работы с ними
 *
 * @author Shishkova Daria Romanovna [dasHko_0], 16IT18K
 * @author Chelnokov Egor Igorevich [llrgvtch], 16IT18K
 */

class Student {
    private ArrayList<Integer> exams;
    private boolean isSocialGrand;
    private static final double socialGrand = 950;

    Student(ArrayList<Integer> exams, boolean isSocialGrand) {
        this.exams = exams;
        this.isSocialGrand = isSocialGrand;
    }

    /**
     * Считает стипендию студента
     *
     * @return grand - общая стипендия студента
     */
    double grand() {
        double grand = 0;
        if (Collections.frequency(exams, 5) == exams.size()) {
            grand = 900 + grand;
        }else if (Collections.frequency(exams, 3) == 0 && Collections.frequency(exams, 2) == 0 && Collections.frequency(exams, 4) > 0) {
            grand = grand + 650;
        }
        if (isSocialGrand) {
            grand = grand + socialGrand;
        }
        return grand;
    }

    @Override
    public String toString() {
        return String.format("Cтудент с итоговыми оценками: %s. Наличие соц.стипендии: %s", exams, isSocialGrand);
    }
}