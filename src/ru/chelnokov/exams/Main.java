package ru.chelnokov.exams;

/**
 * Демокласс для объекта Студент
 *
 * @author Shishkova Daria Romanovna [dasHko_0], 16IT18K
 * @author Chelnokov Egor Igorevich [llrgvtch], 16IT18K
 */

public class Main {
    public static void main(String[] args) {
        Student student = new Student(5, 5, 4, true);//хорошист с социалкой
        Student student1 = new Student(4, 5, 3, true);//без стипендии, но с социалкой
        Student student2 = new Student(5, 5, 5, true);//отличник с социалкой
        Student student3 = new Student(3, 3, 3, false);//ничего не получает
        Student[] students = {student, student1, student2, student3};
        grandOfStudents(students);
    }

    /**
     * выводит оценки и стипендию студентов
     *
     * @param students массив студентов
     */
    private static void grandOfStudents(Student[] students) {
        for (Student grandStudents : students) {
            System.out.print(grandStudents);
            System.out.println(". Общая стипендия: " + grandStudents.grand());
        }
    }
}
